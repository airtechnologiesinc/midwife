import 'dart:async';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';
import 'package:midwife_core/midwife_core.dart' as core;
import 'package:midwife_core/model/enum/user_auth_state.dart';
import 'package:midwife_core/provider/call_provider.dart';
import 'package:midwife_core/provider/inbox_provider.dart';
import 'package:midwife_core/provider/user_provider.dart';
import 'package:midwife_professional/provider/home_provider.dart';
import 'package:midwife_professional/provider/midwife_provider.dart';
import 'package:midwife_professional/res/style.dart';
import 'package:midwife_professional/root.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await core.FirebaseHelper.initFirebaseMobile();

  runZonedGuarded(() {
    runApp(MidwifeProfessional());
  }, (error, stackTrace) {
    print('runZonedGuarded: Caught error in my root zone');
    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
}

class MidwifeProfessional extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => HomeProvider()),
        ChangeNotifierProvider(create: (_) => CallProvider()),
        ChangeNotifierProvider(create: (_) => MidwifeProvider()),
        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => InboxProvider()),
        StreamProvider<UserAuthState>.value(
          value: core.FireAuthHelper.authState(),
          initialData: null,
        ),
      ],
      child: MaterialApp(
        title: 'Midwife Professional',
        theme: ThemeData.from(
            textTheme: kTextTheme,
            colorScheme: FlexColorScheme.light(scheme: FlexScheme.vesuviusBurn)
                .toScheme),
        home: Root(),
      ),
    );
  }
}
