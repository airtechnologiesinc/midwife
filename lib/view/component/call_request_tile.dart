import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:midwife_core/model/entity/wrapped_request.dart';
import 'package:midwife_core/model/enum/call_type.dart';
import 'package:midwife_professional/view/component/call_request_response.dart';

const kRequestIconSide = 55.0;

class CallRequestTile extends StatelessWidget {
  final WrappedRequest request;

  const CallRequestTile(this.request);
  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    final textTheme = Theme.of(context).textTheme;
    return Opacity(
      opacity: request.request.fulfilled ? 0.5 : 1.0,
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  height: kRequestIconSide,
                  width: kRequestIconSide,
                  decoration: BoxDecoration(
                      color: colorScheme.primary.withAlpha(125),
                      borderRadius: BorderRadius.circular(8.0)),
                  child: Icon(
                    request.request.type == CallType.AUDIO
                        ? Icons.call
                        : Icons.video_call_rounded,
                    color: colorScheme.onError,
                  ),
                ),
                SizedBox(width: 8.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "${request.request.firstName} ${request.request.lastName}",
                      style: textTheme.bodyText1.copyWith(
                          fontWeight: FontWeight.bold,
                          color: textTheme.bodyText1.color.withAlpha(175)),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      child: Text(
                        '${DateFormat.yMd().format(request.request.time)}, ${DateFormat.Hms().format(request.request.time)}',
                        style: textTheme.bodyText2.copyWith(
                            fontWeight: FontWeight.bold,
                            color: textTheme.bodyText2.color.withAlpha(125)),
                      ),
                    ),
                    Text(
                      request.request.type.name,
                      style: textTheme.caption.copyWith(
                          fontWeight: FontWeight.bold,
                          color: colorScheme.primary.withAlpha(175)),
                    )
                  ],
                ),
              ],
            ),
            IconButton(
              icon: request.request.fulfilled
                  ? Icon(
                      Icons.check_box_rounded,
                      color: colorScheme.primary,
                    )
                  : Icon(Icons.check_box_outline_blank_rounded),
              onPressed: () {
                showModalBottomSheet(
                    shape: RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(8.0))),
                    context: context,
                    builder: (context) {
                      return CallRequestResponse(request);
                    });
              },
            )
          ],
        ),
      ),
    );
  }
}
