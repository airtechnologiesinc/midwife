import 'package:flutter/material.dart';

const kButtonHeight = 56.0;

class MidwifeTextBtn extends StatelessWidget {
  final String label;
  final VoidCallback onPressed;

  const MidwifeTextBtn(
      {Key key, @required this.label, @required this.onPressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        alignment: Alignment.center,
        height: kButtonHeight,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: Text(
          label,
          style: Theme.of(context).textTheme.button,
        ),
      ),
    );
  }
}
