import 'package:flutter/material.dart';
import 'package:midwife_core/model/entity/wrapped_request.dart';
import 'package:midwife_core/model/enum/call_type.dart';
import 'package:midwife_core/util/functions_helper.dart';
import 'package:midwife_professional/view/component/midwife_text_btn.dart';

class CallRequestResponse extends StatelessWidget {
  const CallRequestResponse(this.request);

  final WrappedRequest request;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 32.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            '${request.request.firstName} ${request.request.lastName} ${request.request.fulfilled ? kRequested : kIsRequesting} ${request.request.type == CallType.AUDIO ? kDeterminerAn : kDeterminerA} ${request.request.type.name.toLowerCase()} $kCall',
            style: textTheme.headline6,
          ),
          SizedBox(
            height: 8.0,
          ),
          MidwifeTextBtn(
              label:
                  request.request.fulfilled ? kMarkIncomplete : kMarkComplete,
              onPressed: () {
                FunctionsHelper.setCallRequestStatus(
                    inbox: request.request.inbox,
                    fulfilled: !(request.request.fulfilled),
                    request: request.id);
                Navigator.pop(context);
              }),
        ],
      ),
    );
  }
}

const kIsRequesting = 'is requesting';
const kRequested = 'requested';
const kDeterminerA = 'a';
const kDeterminerAn = 'an';
const kCall = 'call';
const kMarkComplete = 'Mark Complete';
const kMarkIncomplete = 'Mark Incomplete';
