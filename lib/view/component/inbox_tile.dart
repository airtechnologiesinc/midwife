import 'package:flutter/material.dart';
import 'package:midwife_core/midwife_core.dart';

const kAvatarSide = 48.0;

class InboxTile extends StatelessWidget {
  final Inbox inbox;

  const InboxTile(this.inbox);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  color: Theme.of(context).primaryColor.withAlpha(100)))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            width: kAvatarSide,
            height: kAvatarSide,
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor, shape: BoxShape.circle),
            child: Text(
              inbox.label[0].toUpperCase(),
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(color: Theme.of(context).colorScheme.onPrimary),
            ),
          ),
          SizedBox(width: 16.0),
          Text(
            inbox.label,
            style: Theme.of(context).textTheme.bodyText1,
          )
        ],
      ),
    );
  }
}
