import 'package:flutter/material.dart';
import 'package:midwife_core/model/entity/menu_data.dart';
import 'package:midwife_professional/provider/home_provider.dart';
import 'package:midwife_professional/view/component/menu_option.dart';
import 'package:provider/provider.dart';

class MenuBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<MenuData> data = Provider.of<HomeProvider>(context).menu;
    List<Widget> menuChildren = data
        .asMap()
        .entries
        .map((entry) => GestureDetector(
            onTap: () {
              Provider.of<HomeProvider>(context, listen: false).selected =
                  entry.key;
            },
            child: MenuOption(data: entry.value)))
        .toList();

    return Container(
      height: double.infinity,
      decoration: BoxDecoration(
          // color: Theme.of(context).colorScheme.background,
          border: Border(
              top: BorderSide(
                  width: 1.0,
                  color: Theme.of(context).primaryColor.withAlpha(45)))),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: menuChildren),
    );
  }
}
