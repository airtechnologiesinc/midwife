import 'package:flutter/material.dart';
import 'package:midwife_core/model/entity/menu_data.dart';

const kIconSide = 40.0;

class MenuOption extends StatelessWidget {
  final MenuData data;

  const MenuOption({@required this.data});

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: data.selected == true ? 1.0 : 0.5,
      child: Container(
        width: kIconSide,
        height: kIconSide,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(data.iconPath),
                fit: BoxFit.contain,
                colorFilter: ColorFilter.mode(
                    Theme.of(context).accentColor, BlendMode.srcATop))),
      ),
    );
  }
}
