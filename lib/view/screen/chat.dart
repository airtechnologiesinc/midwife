import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:midwife_core/model/entity/inbox_wrapper.dart';
import 'package:midwife_core/model/enum/inbox_type.dart';
import 'package:midwife_core/provider/call_provider.dart';
import 'package:midwife_core/view/chat_screen.dart';
import 'package:midwife_professional/res/string-en.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class Chat extends StatelessWidget {
  final InboxWrapper wrapped;

  const Chat(this.wrapped);

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        actions: wrapped.inbox.type == InboxType.PUBLIC
            ? []
            : [
                IconButton(
                    icon: Icon(Icons.video_call_rounded),
                    onPressed: () async {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: const Text(kInitiatingVideo),
                        duration: const Duration(seconds: 1),
                        behavior: SnackBarBehavior.floating,
                      ));
                      Provider.of<CallProvider>(context, listen: false)
                          .call(Uuid().v4(), wrapped.id);
                    }),
                // IconButton(icon: Icon(Icons.call), onPressed: () {}),
              ],
        actionsIconTheme: Theme.of(context)
            .accentIconTheme
            .copyWith(color: colorScheme.onSurface),
        iconTheme:
            Theme.of(context).iconTheme.copyWith(color: colorScheme.onSurface),
        title: Text(
          wrapped.inbox.label,
          style: GoogleFonts.montserrat(
              fontWeight: FontWeight.bold,
              fontSize: 18.0,
              color: colorScheme.onSurface),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        centerTitle: true,
      ),
      body: ChatScreen(wrapped.id, wrapped.inbox.type),
    );
  }
}
