import 'package:flutter/material.dart';
import 'package:midwife_core/midwife_core.dart';
import 'package:midwife_core/model/enum/user_type.dart';
import 'package:midwife_professional/res/dev-string.dart';
import 'package:midwife_professional/res/string-en.dart';
import 'package:midwife_professional/res/style.dart';
import 'package:midwife_professional/util/validator.dart';
import 'package:midwife_professional/view/component/midwife_text_btn.dart';

import 'midwife_details.dart';

class NewMidwife extends StatefulWidget {
  final formKey = GlobalKey<FormState>();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();

  @override
  _NewMidwifeState createState() => _NewMidwifeState();
}

class _NewMidwifeState extends State<NewMidwife> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text(
          kAppName,
          style: Theme.of(context).textTheme.subtitle1.copyWith(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold,
              ),
        ),
        centerTitle: false,
      ),
      body: Form(
        key: widget.formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                kDetailsExplained,
                style: Theme.of(context).textTheme.subtitle1,
              ),
              separatorHeight,
              TextFormField(
                validator: Validator.nameValidator,
                controller: widget.firstNameController,
                decoration: getInputDecoration(kFirstNameLabel),
              ),
              SizedBox(
                height: 8.0,
              ),
              TextFormField(
                validator: Validator.nameValidator,
                controller: widget.lastNameController,
                decoration: getInputDecoration(kLastNameLabel),
              ),
              separatorHeight,
              MidwifeTextBtn(
                  label: kContinue,
                  onPressed: () {
                    if (widget.formKey.currentState.validate()) {
                      FireAuthHelper.saveUser(UserType.MIDWIFE,
                          firstName: widget.firstNameController.text,
                          lasName: widget.lastNameController.text);
                    }
                  }),
              SizedBox(
                height: 32.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
