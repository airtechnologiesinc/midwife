import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:midwife_core/midwife_core.dart';
import 'package:midwife_professional/res/string-en.dart';
import 'package:midwife_professional/res/style.dart';
import 'package:midwife_professional/util/validator.dart';
import 'package:midwife_professional/view/component/midwife_text_btn.dart';

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  final _phoneController = TextEditingController();
  final _otpController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  bool otpFieldVisible;
  String _verificationId;

  @override
  void initState() {
    super.initState();
    otpFieldVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          kAppName,
          style: Theme.of(context).textTheme.subtitle1.copyWith(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold),
        ),
        centerTitle: false,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    kWelcomeHeader,
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  Text(
                    kWelcomeText,
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontWeight: FontWeight.normal),
                  ),
                  SizedBox(
                    height: 32.0,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    controller: _phoneController,
                    decoration: getInputDecoration(kPhoneLabel)
                        .copyWith(prefixText: '+233 '),
                    validator: Validator.phoneValidator,
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  otpFieldVisible
                      ? TextFormField(
                          controller: _otpController,
                          decoration: getInputDecoration(kOtpLabel),
                        )
                      : Container(),
                  SizedBox(
                    height: 56.0,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: MidwifeTextBtn(
                          label: kVerify,
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              FireAuthHelper.phoneAuthMobile(
                                  phoneNumber: '+233${_phoneController.text}',
                                  verificationCompleted: (phoneAuthCredential) {
                                    String smsCode =
                                        phoneAuthCredential.smsCode;

                                    setState(() {
                                      otpFieldVisible = true;
                                      _otpController.text = smsCode;
                                    });
                                  },
                                  verificationFailed: (firebaseAuthException) {
                                    FirebaseCrashlytics.instance.log(
                                        'Error signing in ${firebaseAuthException.toString()}');
                                    print(firebaseAuthException.toString());
                                  },
                                  codeSent: (verificationId, resendToken) {
                                    setState(() {
                                      _verificationId = verificationId;
                                      otpFieldVisible = true;
                                    });
                                  },
                                  codeAutoRetrievalTimeout:
                                      (verificationId) {});
                            }
                          },
                        ),
                      ),
                      SizedBox(
                        width: otpFieldVisible ? 28.0 : 0.0,
                      ),
                      otpFieldVisible
                          ? Expanded(
                              child: MidwifeTextBtn(
                                label: kContinue,
                                onPressed: () {
                                  if (_verificationId != null &&
                                      _otpController.text.isNotEmpty) {
                                    PhoneAuthCredential phoneAuthCredential =
                                        PhoneAuthProvider.credential(
                                            verificationId: _verificationId,
                                            smsCode: _otpController.text);
                                    FirebaseAuth.instance.signInWithCredential(
                                        phoneAuthCredential);
                                  } else {
                                    print(
                                        'Verification id null || smsCode is empty');
                                  }
                                },
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
