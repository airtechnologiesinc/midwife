import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:midwife_core/model/entity/wrapped_request.dart';
import 'package:midwife_core/res/string-en.dart';
import 'package:midwife_professional/view/component/call_request_tile.dart';
import 'package:provider/provider.dart';

class AllCallRequests extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    List<WrappedRequest> requests = Provider.of(context);

    if (requests == null) {
      return Center(
        child: Platform.isIOS
            ? CupertinoActivityIndicator()
            : CircularProgressIndicator(),
      );
    }

    if (requests.isEmpty) {
      return Center(
        child: Text(kNoCalls,
            style: textTheme.headline5.copyWith(fontWeight: FontWeight.bold)),
      );
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: ListView.separated(
          itemBuilder: (context, index) {
            return CallRequestTile(requests[index]);
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
          itemCount: requests.length),
    );
  }
}
