import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:midwife_professional/res/dev-string.dart';
import 'package:midwife_professional/res/string-en.dart';
import 'package:midwife_professional/res/style.dart';
import 'package:midwife_professional/util/store_helper.dart';
import 'package:midwife_professional/util/validator.dart';
import 'package:midwife_professional/view/component/midwife_text_btn.dart';

const separatorHeight = SizedBox(
  height: 16.0,
);
const _kFieldHeight = 58.0;

const kTrue = 'True';
const kFalse = 'False';

class MidwifeDetails extends StatefulWidget {
  MidwifeDetails({Key key}) : super(key: key);

  @override
  _MidwifeDetailsState createState() => _MidwifeDetailsState();
}

class _MidwifeDetailsState extends State<MidwifeDetails> {
  final _formKey = GlobalKey<FormState>();
  final _pinController = TextEditingController();
  final _locationController = TextEditingController();
  final _emergencyController = TextEditingController();

  DateTime _dOB;

  @override
  void initState() {
    super.initState();
    _dOB = DateTime.now();
  }

  @override
  void dispose() {
    _pinController.dispose();
    _locationController.dispose();
    _emergencyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text(
          kAppName,
          style: Theme.of(context).textTheme.subtitle1.copyWith(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold,
              ),
        ),
        centerTitle: false,
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: ListView(
            children: [
              Text(
                kDetailsExplained,
                style: Theme.of(context).textTheme.subtitle1,
              ),
              separatorHeight,
              Text(kDOBLabel),
              GestureDetector(
                onTap: () async {
                  DateTime date = await _selectDate(
                    kDOBPickerTitle,
                    DateTime.now().subtract(
                      Duration(
                        days: 36500,
                      ),
                    ),
                  );
                  if (date != null) {
                    setState(() {
                      _dOB = date;
                    });
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Container(
                    padding: const EdgeInsets.only(left: 16.0),
                    alignment: Alignment.centerLeft,
                    height: _kFieldHeight,
                    decoration: getBoxDecoration(),
                    child: Text(
                      DateFormat.yMd().format(_dOB),
                      style: Theme.of(context).textTheme.bodyText1.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                    ),
                  ),
                ),
              ),
              separatorHeight,
              TextFormField(
                validator: Validator.pinValidator,
                controller: _pinController,
                decoration: getInputDecoration(kPinLabel),
              ),
              separatorHeight,
              TextFormField(
                validator: Validator.phoneValidator,
                controller: _emergencyController,
                decoration: getInputDecoration(kEmergencyContactLabel),
              ),
              separatorHeight,
              TextFormField(
                validator: Validator.nameValidator,
                controller: _locationController,
                decoration: getInputDecoration(kLocationLabel),
              ),
              separatorHeight,
              MidwifeTextBtn(
                  label: kContinue,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      StoreHelper.saveMidwife(
                          dob: _dOB,
                          location: _locationController.text,
                          pin: _pinController.text,
                          emergencyContact: _emergencyController.text);
                    }
                  }),
              SizedBox(
                height: 32.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<DateTime> _selectDate(String helpText, DateTime firstDate) async {
    return await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: firstDate,
        lastDate: DateTime.now(),
        helpText: helpText.toString());
  }
}
