import 'package:flutter/material.dart';
import 'package:midwife_core/model/enum/menu_data_type.dart';
import 'package:midwife_professional/view/screen/all_call_requests.dart';
import 'package:midwife_professional/view/screen/chats.dart';
import 'package:midwife_professional/view/screen/settings_screen.dart';

class HomeScreen extends StatelessWidget {
  final MenuDataType type;

  const HomeScreen({this.type});

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case MenuDataType.CALLS:
        return AllCallRequests();

      case MenuDataType.CHAT:
        return Chats();

      case MenuDataType.SETTINGS:
        return SettingsScreen();
    }

    return Container();
  }
}
