import 'package:flutter/material.dart';
import 'package:midwife_core/midwife_core.dart';
import 'package:midwife_core/model/enum/inbox_type.dart';
import 'package:midwife_core/provider/inbox_provider.dart';
import 'package:midwife_core/provider/user_provider.dart';
import 'package:provider/provider.dart';

import 'chat.dart';

const kPrivateChat = 'Your Patients';
const kPublicChat = 'Your Groups';
const kYourMidwife = 'Your Midwife';

_vH(double h) => SliverToBoxAdapter(
      child: Container(height: h),
    );

class ChatList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Provider.of<InboxProvider>(context, listen: false).onInboxTileTapped =
        (wrapped) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Chat(wrapped)),
      );
    };

    final user = Provider.of<UserProvider>(context).user;
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;

    final headerStyle = textTheme.headline6
        .copyWith(fontSize: 16.0, color: colorScheme.primary);

    List<String> privateInboxes =
        user.private == null ? [] : user.private.toList();
    List<String> groups = user.public == null ? [] : user.public.toList();

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Text(
              kPrivateChat,
              style: headerStyle,
            ),
          ),
          _vH(8.0),
          InboxListSliver(InboxType.PRIVATE, privateInboxes),
          _vH(32.0),
          SliverToBoxAdapter(
            child: Text(
              kPublicChat,
              style: headerStyle,
            ),
          ),
          _vH(8.0),
          InboxListSliver(InboxType.PUBLIC, groups),
        ],
      ),
    );
  }
}
