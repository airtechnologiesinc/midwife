import 'package:flutter/material.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;

    return Scaffold(
        body: Center(
      child: CircularProgressIndicator(
        backgroundColor: colorScheme.primary.withAlpha(100),
        valueColor: AlwaysStoppedAnimation<Color>(colorScheme.primary),
      ),
    ));
  }
}
