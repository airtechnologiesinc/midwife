import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:midwife_core/model/enum/menu_data_type.dart';
import 'package:midwife_professional/provider/home_provider.dart';
import 'package:midwife_professional/view/component/menu_bar.dart';
import 'package:provider/provider.dart';

import 'home_screen.dart';

const kBarHeight = 60.0;

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MenuDataType type = (Provider.of<HomeProvider>(context).menu)
        .firstWhere((data) => data.selected == true)
        .type;
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text(
          Provider.of<HomeProvider>(context).title,
        ),
        textTheme: Theme.of(context).textTheme,
        actions: [
          IconButton(
              onPressed: () {
                FirebaseAuth.instance.signOut();
              },
              icon: Icon(
                Icons.power_settings_new,
                color: Theme.of(context).colorScheme.error,
              ))
        ],
        actionsIconTheme: Theme.of(context).iconTheme,
      ),
      body: Stack(
        children: [
          Positioned(
              top: 0.0,
              child: Container(
                width: width,
                height: height -
                    kBarHeight -
                    kToolbarHeight -
                    MediaQuery.of(context).padding.top,
                child: HomeScreen(type: type),
              )),
          Positioned(
              bottom: 0.0,
              child: Container(
                width: width,
                height: kBarHeight,
                child: MenuBar(),
              ))
        ],
      ),
    );
  }
}
