import 'package:flutter/material.dart';
import 'package:midwife_professional/res/dev-string.dart';
import 'package:midwife_professional/res/string-en.dart';

const kInactiveIconSide = 125.0;

class Inactive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: kInactiveIconSide,
                width: kInactiveIconSide,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(
                      kInactiveIcon,
                    ),
                    colorFilter: ColorFilter.mode(
                      Theme.of(context).colorScheme.onBackground,
                      BlendMode.srcATop,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 32.0,
              ),
              Text(
                kInactiveErrorHeader,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline6.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
              ),
              SizedBox(
                height: 32.0,
              ),
              Text(
                kInactiveErrorTxt,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
