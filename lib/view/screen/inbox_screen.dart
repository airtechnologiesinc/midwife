import 'package:flutter/material.dart';
import 'package:midwife_core/provider/call_provider.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class InboxScreen extends StatelessWidget {
  final String inbox;
  final String label;

  const InboxScreen(this.inbox, this.label);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(label),
          actions: [
            // IconButton(
            //     icon: Icon(Icons.call),
            //     onPressed: () {
            //       AgoraHelper().call(Uuid().v1(), inbox);
            //     }),
            IconButton(
                icon: Icon(Icons.video_call),
                onPressed: () {
                  Provider.of<CallProvider>(context, listen: false)
                      .call(Uuid().v1(), inbox);
                }),
          ],
        ),
        body: Container());
  }
}
