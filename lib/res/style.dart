import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final kTextTheme = TextTheme(
  headline1: GoogleFonts.ovo(
      fontSize: 109, fontWeight: FontWeight.w300, letterSpacing: -1.5),
  headline2: GoogleFonts.ovo(
      fontSize: 68, fontWeight: FontWeight.w300, letterSpacing: -0.5),
  headline3: GoogleFonts.ovo(fontSize: 55, fontWeight: FontWeight.w400),
  headline4: GoogleFonts.ovo(
      fontSize: 39, fontWeight: FontWeight.w400, letterSpacing: 0.25),
  headline5: GoogleFonts.ovo(fontSize: 27, fontWeight: FontWeight.w400),
  headline6: GoogleFonts.ovo(
      fontSize: 23, fontWeight: FontWeight.w500, letterSpacing: 0.15),
  subtitle1: GoogleFonts.ovo(
      fontSize: 18, fontWeight: FontWeight.w400, letterSpacing: 0.15),
  subtitle2: GoogleFonts.ovo(
      fontSize: 16, fontWeight: FontWeight.w500, letterSpacing: 0.1),
  bodyText1: GoogleFonts.quattrocentoSans(
      fontSize: 18, fontWeight: FontWeight.w400, letterSpacing: 0.5),
  bodyText2: GoogleFonts.quattrocentoSans(
      fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.25),
  button: GoogleFonts.quattrocentoSans(
      fontSize: 16, fontWeight: FontWeight.w500, letterSpacing: 1.25),
  caption: GoogleFonts.quattrocentoSans(
      fontSize: 14, fontWeight: FontWeight.w400, letterSpacing: 0.4),
  overline: GoogleFonts.quattrocentoSans(
      fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 1.5),
);

InputDecoration getInputDecoration(String labelText) {
  return InputDecoration(
    labelText: labelText,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(12),
    ),
  );
}

BoxDecoration getBoxDecoration() {
  return BoxDecoration(
      borderRadius: BorderRadius.circular(12), border: Border.all(width: 0.5));
}
