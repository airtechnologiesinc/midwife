const kAppName = 'Moms & Midwives';
const kWelcomeHeader = 'Welcome hero.\nLet\'s get started';
const kWelcomeText = 'Enter your phone number to begin';
const kPhoneLabel = 'Phone Number';
const kOtpLabel = 'Six digit pin';
const kVerify = 'Verify';
const kContinue = 'Continue';
const kInactiveErrorHeader =
    'Oops. It seems your account has not been activated';
const kInactiveErrorTxt = 'Shoot us an email. Let\'s get you started ASAP';
const kInitiatingVideo = 'Initiating Video Call';
