import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:midwife_core/model/entity/call_request.dart';
import 'package:midwife_core/model/entity/wrapped_request.dart';
import 'package:midwife_core/model/serializer.dart';
import 'package:midwife_core/res/dev_string.dart';
import 'package:midwife_professional/res/dev-string.dart';

class StoreHelper {
  static final _store = FirebaseFirestore.instance;

  static Future saveMidwife(
      {@required DateTime dob,
      @required String location,
      @required String pin,
      @required String emergencyContact}) async {
    try {
      String uid = FirebaseAuth.instance.currentUser.uid;

      await _store.collection(kMidwifeDetails).doc(uid).set({
        kMidwifeStatus: false,
        kPinKey: pin,
        kDOBKey: dob,
        kLocationKey: location,
        kEmergencyContactKey: emergencyContact,
      });
    } catch (e) {
      print(e);
    }
  }

  static Stream<List<WrappedRequest>> requests() {
    return FirebaseFirestore.instance
        .collectionGroup(kCallRequestCollection)
        .where(kMidwivesKey,
            arrayContains: FirebaseAuth.instance.currentUser.uid)
        .orderBy(kFulfilledKey, descending: false)
        .orderBy(kRequestTimeKey, descending: true)
        .snapshots()
        .map((event) => event.docs.map((doc) {
              final request = serializers.deserializeWith(
                  CallRequest.serializer, doc.data());
              return WrappedRequest((b) => b
                ..request = request.toBuilder()
                ..id = doc.id);
            }).toList());
  }
}

const kRequestTimeKey = 'time';
const kFulfilledKey = 'fulfilled';
