const kInvalidName = 'Name must be at least 3 characters long';
const kInvalidPhone = 'You must enter a valid phone number';
const kInvalidCode = 'SMS code must be 6 characters long';
const kInvalidPositiveInteger =
    'Please enter a valid positive integer. Enter \'0\' if none.';
const kInvalidPin = 'Please enter a valid PIN';

class Validator {
  static String nameValidator(String name) {
    if (name == null || name.length < 3) {
      return kInvalidName;
    }
    return null;
  }

  static String intValidator(String value) {
    if (int.tryParse(value) == null || int.tryParse(value) < 0) {
      return kInvalidPositiveInteger;
    }
    return null;
  }

  static String phoneValidator(String phone) {
    if (int.tryParse(phone) == null) {
      return kInvalidPhone;
    }

    if (phone.isEmpty) {
      return kInvalidPhone;
    }

    return null;
  }

  static String smsCodeValidator(String smsCode) {
    if (smsCode == null || smsCode.isEmpty) {
      return kInvalidCode;
    }

    if (int.tryParse(smsCode) == null) {
      return kInvalidCode;
    }

    return null;
  }

  static String pinValidator(String pin) {
    if (pin == null || pin.isEmpty) {
      return kInvalidPin;
    }

    return null;
  }
}
