import 'package:flutter/material.dart';
import 'package:midwife_core/model/entity/menu_data.dart';
import 'package:midwife_core/model/enum/menu_data_type.dart';
import 'package:midwife_professional/res/dev-string.dart';

class HomeProvider with ChangeNotifier {
  String _homeTitle = 'Chat';
  String get title => _homeTitle;

  final List<MenuData> _menu = [
    MenuData((b) => b
      ..iconPath = kChatIcon
      ..type = MenuDataType.CHAT
      ..label = 'Chat'
      ..selected = true),
    MenuData((b) => b
      ..iconPath = kCallIcon
      ..type = MenuDataType.CALLS
      ..label = 'Call Requests'
      ..selected = false),
    // MenuData((b) => b
    //   ..iconPath = kSettingsIcon
    //   ..type = MenuDataType.SETTINGS
    //   ..label = 'Settings'
    //   ..selected = false),
  ];

  List<MenuData> get menu => _menu;

  set selected(int index) {
    try {
      int oldSelectedIndex = _menu.indexWhere((data) => data.selected == true);
      MenuData oldSelected = _menu[oldSelectedIndex];
      _menu[oldSelectedIndex] = MenuData((b) => b
        ..selected = false
        ..iconPath = oldSelected.iconPath
        ..type = oldSelected.type
        ..label = oldSelected.label);

      MenuData newSelected = _menu[index];
      _menu[index] = MenuData((b) => b
        ..selected = true
        ..type = newSelected.type
        ..iconPath = newSelected.iconPath
        ..label = newSelected.label);

      _homeTitle = menu[index].label;

      notifyListeners();
    } catch (e) {
      print(e.toString());
    }
  }
}
