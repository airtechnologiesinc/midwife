import 'package:flutter/cupertino.dart';
import 'package:midwife_core/midwife_core.dart';

class MidwifeProvider extends ChangeNotifier {
  MidwifeDetails _details;

  MidwifeDetails get details => _details;

  set details(MidwifeDetails d) {
    _details = d;
    notifyListeners();
  }

  init(MidwifeDetails d) {
    _details = d;
  }
}
