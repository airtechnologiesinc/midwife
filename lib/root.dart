import 'package:flutter/material.dart';
import 'package:midwife_core/midwife_core.dart' as core;
import 'package:midwife_core/model/entity/wrapped_request.dart';
import 'package:midwife_core/model/enum/user_auth_state.dart';
import 'package:midwife_core/provider/call_provider.dart';
import 'package:midwife_core/provider/user_provider.dart';
import 'package:midwife_core/util/firebase_helper.dart';
import 'package:midwife_core/view/call_handler.dart';
import 'package:midwife_professional/provider/midwife_provider.dart';
import 'package:midwife_professional/util/store_helper.dart';
import 'package:midwife_professional/view/screen/home.dart';
import 'package:midwife_professional/view/screen/inactive.dart';
import 'package:midwife_professional/view/screen/new_midwife.dart';
import 'package:midwife_professional/view/screen/splash.dart';
import 'package:midwife_professional/view/screen/welcome.dart';
import 'package:provider/provider.dart';

import 'view/screen/midwife_details.dart';

class Root extends StatelessWidget {
  final OverlayEntry callOverlayEntry = OverlayEntry(
      builder: (context) => Material(
            child: CallHandler(),
          ));
  @override
  Widget build(BuildContext context) {
    UserAuthState state = Provider.of(context);

    bool incoming = Provider.of<CallProvider>(context).incoming ?? false;
    bool onCall = Provider.of<CallProvider>(context).onCall ?? false;

    if (incoming || onCall) {
      // Overlay.of(context).insert(callOverlayEntry);
      WidgetsBinding.instance.addPostFrameCallback(
          (timeStamp) => Overlay.of(context).insert(callOverlayEntry));
    } else {
      if (callOverlayEntry.mounted) {
        callOverlayEntry.remove();
      }
    }

    if (state == null) {
      return Splash();
    }

    return state == UserAuthState.UNAUTHENTICATED
        ? Welcome()
        : MultiProvider(
            providers: [
              StreamProvider<core.User>.value(
                value: core.FireAuthHelper.user(),
                initialData: null,
              ),
              StreamProvider<bool>.value(
                value: core.FireAuthHelper.midwifeStatus(),
                initialData: null,
              ),
              StreamProvider<core.MidwifeDetails>.value(
                value: core.FireAuthHelper.midwifeDetails(),
                initialData: null,
              ),
              StreamProvider<List<WrappedRequest>>.value(
                value: StoreHelper.requests(),
                initialData: null,
              )
            ],
            child: _StatusHandler(),
          );
  }
}

class _StatusHandler extends StatefulWidget {
  @override
  __StatusHandlerState createState() => __StatusHandlerState();
}

class __StatusHandlerState extends State<_StatusHandler> {
  @override
  void initState() {
    super.initState();
    FirebaseHelper.saveTokenToDatabase();
  }

  @override
  Widget build(BuildContext context) {
    core.MidwifeDetails details = Provider.of(context);
    bool active = Provider.of<bool>(context) ?? false;
    core.User user = Provider.of(context);

    Provider.of<UserProvider>(context).init(user);
    Provider.of<MidwifeProvider>(context).init(details);

    if (user == null) {
      return Splash();
    }
    if (user.firstName == null ||
        user.lastName == null ||
        user.userType == null) {
      return NewMidwife();
    }
    if (details == null) {
      return Splash();
    }
    if (details.dateOfBirth == null ||
        details.dateOfBirth == null ||
        details.location == null) {
      return MidwifeDetails();
    }

    if (!active) {
      return Inactive();
    }
    return Home();
  }
}
